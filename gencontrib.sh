#!/bin/sh

contrib()
{
    author="$1"
    rev="$2"

    echo Real author: $author >&2
    git log -1 --format=fuller "$rev" >&2
    echo >&2
    git log -1 --format=tformat:%B "$rev" \
    | sed -n 's/Rev: \([^:]*\):\([0-9.]*\)$/\1	\2	'"$author/p"
}

contrib mani "master^{/Several year accumulated}"
contrib mani "master^{/Several years worth of}"
contrib nisse "master^{/Debug error printout bugfix from Nield}"
contrib grubba "master^{/Two patches from grubba}"
contrib cardeci "master^{/Simple HTTP PUT client written by Per Hedbor}"
contrib grubba "master^{/Prepend timestamps to messages, to improve logging}"
contrib marcus "master^{/NetBSD fixes supplied by Marcus Comstedt}"
contrib ceder "master^{/YYY -> YYYY. Thanks to ceder.}"
