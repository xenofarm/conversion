#!/bin/sh

# Fix UTF-8 errors.  The errors were detected with this one-liner:
#
# find . -type f -name \*,v| while read i ; do rlog $i | recode utf-8..latin-1 >/dev/null || echo $i ; done
#
# And then some other errors were found with:
#
# git log | recode utf-8..latin-1 

set -e

rcs -m1.1:'Första incheckningen av Xnakefarm-script.' cvsroot/xenofarm/projects/python/result-mailer.py,v
rcs -m1.2:'Tagit bort variant-hanteringen; client.sh från zino löser problemet
snyggare.' cvsroot/xenofarm/projects/python/result-mailer.py,v

rcs -m1.2:'Tagit bort variant-hanteringen; client.sh från zino löser problemet
snyggare.' cvsroot/xenofarm/projects/python/Attic/source-transform.sh,v
rcs -m1.1:'Första incheckningen av Xnakefarm-script.' cvsroot/xenofarm/projects/python/Attic/source-transform.sh,v

rcs -m1.1:'Första incheckningen av Xnakefarm-script.' cvsroot/xenofarm/projects/python/Attic/server.pike,v

rcs -m1.1:'Första incheckningen av Xnakefarm-script.' cvsroot/xenofarm/projects/python/Attic/result_parser.pike,v
