all: s/xenofarm-pcvs2git.git.stamp \
	s/xenofarm-cvs2git.git.stamp \
	s/xenofarm-cvsimport.git.stamp

s/xenofarm-pcvs2git.git.stamp: s/pcvs2git-fixed.stamp s/cvsroot.stamp authors contrib.txt
	rm -rf xenofarm-pcvs2git.git
	pike pcvs2git/pcvs2git.pike --disable-rev -C xenofarm-pcvs2git.git -o master --contributors contrib.txt -A authors -d cvsroot/xenofarm
	touch $@

contrib.txt: s/xenofarm-pcvs2git-with-rev.git.stamp gencontrib.sh
	(cd xenofarm-pcvs2git-with-rev.git && ../gencontrib.sh) > $@.tmp
	mv -f $@.tmp $@

s/xenofarm-pcvs2git-with-rev.git.stamp: s/pcvs2git-fixed.stamp s/cvsroot.stamp authors
	rm -rf xenofarm-pcvs2git.git
	pike pcvs2git/pcvs2git.pike -C xenofarm-pcvs2git-with-rev.git -o master -A authors -d cvsroot/xenofarm
	touch $@

s/pcvs2git.stamp:
	git clone git://pike-git.lysator.liu.se/pcvs2git.git
	touch $@

s/pcvs2git-fixed.stamp: s/pcvs2git.stamp
	: No pending fixes at the moment.
	touch $@

s/cvsroot.stamp: s/cvsroot.nolog.stamp fix-utf8.sh
	./fix-utf8.sh
	touch $@

s/cvsroot.nolog.stamp: s/cvsroot.scp.stamp
	rm -f cvsroot/xenofarm/projects/pike/log_*,v
	touch $@

s/cvsroot.scp.stamp:
	rm -rf cvsroot
	scp -r ceder@cvs.lysator.liu.se:/cvsroot/xenofarm cvsroot
	touch $@

s/xenofarm-cvs2git.git.stamp: authors.sed s/git-blob.dat.stamp
	rm -rf xenofarm-cvs2git.git
	(cat git-blob.dat; sed -f ./authors.sed < git-dump.dat) | (mkdir xenofarm-cvs2git.git && cd xenofarm-cvs2git.git && git init --bare && git fast-import)
	touch $@

authors.sed: authors
	rm -f $@ $@.tmp
	sed -e 's/^\([^=]*\)=\(.*\)$$/s%^committer \1 <\1>%committer \2%/' authors > $@.tmp
	mv $@.tmp $@

s/git-blob.dat.stamp: s/cvsroot.stamp
	rm -f git-blob.dat git-dump.dat
	cvs2git \
	    --encoding=utf-8 \
	    --blobfile=git-blob.dat \
	    --dumpfile=git-dump.dat \
	    --username=cvs2git \
	    cvsroot/xenofarm
	touch $@

s/xenofarm-cvsimport.git.stamp: s/cvsroot.stamp authors
	rm -rf xenofarm-cvsimport.git
	git cvsimport -p -x -d `pwd`/cvsroot xenofarm -C xenofarm-cvsimport.git -o master -a -A authors -R
	touch $@

check: s/check-pcvs-master.stamp \
	s/check-pcvs-1.0.stamp \
	s/check-pcvs-1.2.stamp \
	s/check-pcvs-1.3.stamp\
	s/check-cvs-master.stamp \
	s/check-cvs-1.0.stamp \
	s/check-cvs-1.2.stamp \
	s/check-cvs-1.3.stamp \
	s/check-imp-master.stamp \
	s/check-imp-1.0.stamp \
	s/check-imp-1.2.stamp \
	s/check-imp-1.3.stamp \
	s/compare-commits.stamp

s/check-pcvs-master.stamp: s/cvs-master.stamp s/pcvs-master.stamp
	diff -u -x CVS -x .git -x .gitignore -x .gitattributes -r \
	    sandbox/cvs/master/xenofarm sandbox/pcvs/master
	touch $@

s/check-pcvs-1.0.stamp: s/cvs-1.0.stamp s/pcvs-1.0.stamp
	diff -u -x CVS -x .git -x .gitignore -x .gitattributes -r \
	    sandbox/cvs/1.0/xenofarm sandbox/pcvs/1.0
	touch $@

s/check-pcvs-1.2.stamp: s/cvs-1.2.stamp s/pcvs-1.2.stamp
	diff -u -x CVS -x .git -x .gitignore -x .gitattributes -r \
	    sandbox/cvs/1.2/xenofarm sandbox/pcvs/1.2
	touch $@

s/check-pcvs-1.3.stamp: s/cvs-1.3.stamp s/pcvs-1.3.stamp
	diff -u -x CVS -x .git -x .gitignore -x .gitattributes -r \
	    sandbox/cvs/1.3/xenofarm sandbox/pcvs/1.3
	touch $@

s/check-cvs-master.stamp: s/cvs-master.stamp s/gcvs-master.stamp
	diff -u -x CVS -x .git -x .gitignore -x .gitattributes -r \
	    sandbox/cvs/raw/master/xenofarm sandbox/gcvs/master
	touch $@

s/check-cvs-1.0.stamp: s/cvs-1.0.stamp s/gcvs-1.0.stamp
	diff -u -x CVS -x .git -x .gitignore -x .gitattributes -r \
	    sandbox/cvs/raw/1.0/xenofarm sandbox/gcvs/1.0
	touch $@

s/check-cvs-1.2.stamp: s/cvs-1.2.stamp s/gcvs-1.2.stamp
	diff -u -x CVS -x .git -x .gitignore -x .gitattributes -r \
	    sandbox/cvs/raw/1.2/xenofarm sandbox/gcvs/1.2
	touch $@

s/check-cvs-1.3.stamp: s/cvs-1.3.stamp s/gcvs-1.3.stamp
	diff -u -x CVS -x .git -x .gitignore -x .gitattributes -r \
	    sandbox/cvs/raw/1.3/xenofarm sandbox/gcvs/1.3
	touch $@

s/check-imp-master.stamp: s/cvs-master.stamp s/imp-master.stamp
	diff -u -x CVS -x .git -x .gitignore -x .gitattributes -r \
	    sandbox/cvs/raw/master/xenofarm sandbox/imp/master
	touch $@

s/check-imp-1.0.stamp: s/cvs-1.0.stamp s/imp-1.0.stamp
	diff -u -x CVS -x .git -x .gitignore -x .gitattributes -r \
	    sandbox/cvs/raw/1.0/xenofarm sandbox/imp/1.0
	touch $@

s/check-imp-1.2.stamp: s/cvs-1.2.stamp s/imp-1.2.stamp
	diff -u -x CVS -x .git -x .gitignore -x .gitattributes -r \
	    sandbox/cvs/raw/1.2/xenofarm sandbox/imp/1.2
	touch $@

s/check-imp-1.3.stamp: s/cvs-1.3.stamp s/imp-1.3.stamp
	diff -u -x CVS -x .git -x .gitignore -x .gitattributes -r \
	    sandbox/cvs/raw/1.3/xenofarm sandbox/imp/1.3
	touch $@

s/cvs-master.stamp: s/cvs-raw-master.stamp fix-id
	./fix-id master
	touch $@

s/cvs-raw-master.stamp: s/cvsroot.stamp
	rm -rf sandbox/cvs/raw/master
	mkdir -p sandbox/cvs/raw
	cd sandbox/cvs/raw && cvs -d `pwd`/../../../cvsroot co -d master/xenofarm xenofarm
	touch $@

s/cvs-1.0.stamp: s/cvs-raw-1.0.stamp fix-id
	./fix-id 1.0
	touch $@

s/cvs-raw-1.0.stamp: s/cvsroot.stamp
	rm -rf sandbox/cvs/raw/1.0
	mkdir -p sandbox/cvs/raw
	cd sandbox/cvs/raw && cvs -d `pwd`/../../../cvsroot co -d 1.0/xenofarm -r xenoclient_1_0 xenofarm
	touch $@

s/cvs-1.2.stamp: s/cvs-raw-1.2.stamp fix-id
	./fix-id 1.2
	touch $@

s/cvs-raw-1.2.stamp: s/cvsroot.stamp
	rm -rf sandbox/cvs/raw/1.2
	mkdir -p sandbox/cvs/raw
	cd sandbox/cvs/raw && cvs -d `pwd`/../../../cvsroot co -d 1.2/xenofarm -r xenoclient_1_2 xenofarm
	touch $@

s/cvs-1.3.stamp: s/cvs-raw-1.3.stamp fix-id
	./fix-id 1.3
	touch $@

s/cvs-raw-1.3.stamp: s/cvsroot.stamp
	rm -rf sandbox/cvs/raw/1.3
	mkdir -p sandbox/cvs/raw
	cd sandbox/cvs/raw && cvs -d `pwd`/../../../cvsroot co -d 1.3/xenofarm -r xenoclient_1_3 xenofarm
	touch $@

s/pcvs-master.stamp: s/xenofarm-pcvs2git.git.stamp
	rm -rf sandbox/pcvs/master
	mkdir -p sandbox/pcvs
	git clone xenofarm-pcvs2git.git sandbox/pcvs/master
	touch $@

s/pcvs-1.0.stamp: s/xenofarm-pcvs2git.git.stamp
	rm -rf sandbox/pcvs/1.0
	mkdir -p sandbox/pcvs
	git clone -b xenoclient_1.0 xenofarm-pcvs2git.git sandbox/pcvs/1.0
	touch $@

s/pcvs-1.2.stamp: s/xenofarm-pcvs2git.git.stamp
	rm -rf sandbox/pcvs/1.2
	mkdir -p sandbox/pcvs
	git clone -b xenoclient_1.2 xenofarm-pcvs2git.git sandbox/pcvs/1.2
	touch $@

s/pcvs-1.3.stamp: s/xenofarm-pcvs2git.git.stamp
	rm -rf sandbox/pcvs/1.3
	mkdir -p sandbox/pcvs
	git clone -b xenoclient_1.3 xenofarm-pcvs2git.git sandbox/pcvs/1.3
	touch $@

s/gcvs-master.stamp: s/xenofarm-cvs2git.git.stamp
	rm -rf sandbox/gcvs/master
	mkdir -p sandbox/gcvs
	git clone xenofarm-cvs2git.git sandbox/gcvs/master
	touch $@

s/gcvs-1.0.stamp: s/xenofarm-cvs2git.git.stamp
	rm -rf sandbox/gcvs/1.0
	mkdir -p sandbox/gcvs
	git clone -b xenoclient_1_0 xenofarm-cvs2git.git sandbox/gcvs/1.0
	touch $@

s/gcvs-1.2.stamp: s/xenofarm-cvs2git.git.stamp
	rm -rf sandbox/gcvs/1.2
	mkdir -p sandbox/gcvs
	git clone -b xenoclient_1_2 xenofarm-cvs2git.git sandbox/gcvs/1.2
	touch $@

s/gcvs-1.3.stamp: s/xenofarm-cvs2git.git.stamp
	rm -rf sandbox/gcvs/1.3
	mkdir -p sandbox/gcvs
	git clone -b xenoclient_1_3 xenofarm-cvs2git.git sandbox/gcvs/1.3
	touch $@

s/imp-master.stamp: s/xenofarm-cvsimport.git.stamp
	rm -rf sandbox/imp/master
	mkdir -p sandbox/imp
	git clone xenofarm-cvsimport.git sandbox/imp/master
	touch $@

s/imp-1.0.stamp: s/xenofarm-cvsimport.git.stamp
	rm -rf sandbox/imp/1.0
	mkdir -p sandbox/imp
	git clone -b xenoclient_1_0 xenofarm-cvsimport.git sandbox/imp/1.0
	touch $@

s/imp-1.2.stamp: s/xenofarm-cvsimport.git.stamp
	rm -rf sandbox/imp/1.2
	mkdir -p sandbox/imp
	git clone -b xenoclient_1_2 xenofarm-cvsimport.git sandbox/imp/1.2
	touch $@

s/imp-1.3.stamp: s/xenofarm-cvsimport.git.stamp
	rm -rf sandbox/imp/1.3
	mkdir -p sandbox/imp
	git clone -b xenoclient_1_3 xenofarm-cvsimport.git sandbox/imp/1.3
	touch $@

s/compare-commits.stamp: s/compare-cvsimport.stamp s/compare-gcvs.stamp
	touch $@

s/compare-cvsimport.stamp: cvsimport-diff
	cmp cvsimport-diff cvsimport-diff.expected
	touch $@

cvsimport-diff: pcvs.log cvsimport.log
	-diff -u cvsimport.log pcvs.log | sed -e 's/@@.*/@@/' -e 1,2d >$@.tmp
	mv -f $@.tmp $@

s/compare-gcvs.stamp: gcvs-diff
	cmp gcvs-diff gcvs-diff.expected
	touch $@

gcvs-diff: pcvs.log gcvs.log
	-diff -u gcvs.log pcvs.log | sed -e 's/@@.*/@@/' -e 1,2d >$@.tmp
	mv -f $@.tmp $@

pcvs.log: s/xenofarm-pcvs2git.git.stamp
	(cd xenofarm-pcvs2git.git && git log --pretty='tformat:%s%b') > $@.tmp
	mv -f $@.tmp $@

gcvs.log: s/xenofarm-cvs2git.git.stamp
	(cd xenofarm-cvs2git.git && git log --pretty='tformat:%s%b') > $@.tmp
	mv -f $@.tmp $@

cvsimport.log: s/xenofarm-cvsimport.git.stamp
	(cd xenofarm-cvsimport.git && git log --pretty='tformat:%s%b') > $@.tmp
	mv -f $@.tmp $@
